package databaseproject

class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/"(view:"/index", model: [user: null, loginClicked: false])
        "500"(view:'/error')
        "404"(view:'/notFound')
    }
}
