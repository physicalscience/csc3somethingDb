package databaseproject

import groovy.json.JsonSlurper

class GameController {
    def userLoginService
    def index() { }

    def search() {
        def name = params.get("gameName")
        def foundGame = Game.findByName(name.toString())

        if(foundGame) {
            render(view: 'foundGame', model: [game: foundGame])
        }
        else {
            def safeName = name.toString().replace(" ", "%20")
            def connection = new URL("https://www.giantbomb.com/api/search/?api_key=e8a3ee2589f8db0821210188ca6e7181f83ac104" +
                    "&format=json&query=" + '"' + safeName  + '"' + "&resources=game").openConnection() as HttpURLConnection

            connection.setRequestProperty("User-Agent", "physicalscience")
            connection.connect()

            def code = connection.responseCode
            if(code) {
                println code
                try {
                    def json = new JsonSlurper()
                    def game = json.parse(connection.inputStream)
                    def allGames = game.results
                    for( games in allGames) {
                        def genre = new Genre(title: games.api_detail_url)
                        genre.save(flush: true, failOnError: true)
                        String shortDeck = games.deck
                        def s
                        if(shortDeck.size() > 255) {
                            s = shortDeck.substring(0, 255)
                        } else {
                            s = shortDeck
                        }
                        def g = new Game(name: games.name, deck: s, genre: genre)
                        for(platforms in game.platorms) {
                            g.addToPlatforms(new Platform(title: platforms.name))
                        }
                        g.save(flush: true, failOnError: true)
                    }
                    println "about to render!"
                    render(view: 'foundGame', model: [games: allGames])
                } catch(IOException e) {
                    render(view: 'view')
                }

            }

        }
    }

    def loadSearch() {
        render(view: 'view')
    }

    def loadBrowse() {
        def games = Game.all
        User user = userLoginService.getUser()
        if(user) {
            render(view: 'browse', model: [dbGames: games, user: user, userGames: user.games])
        } else {
            render(view: 'browse', model: [dbGames: games, user: null, userGames: null])
        }

    }

    def tagPage() {
        Game game = Game.findById(Integer.parseInt(params.get("game").toString().substring(params.get("game").toString().size() -1)))
        render view: 'tagFind', model: [game: game]
    }

    def addTag() {
        Game game = Game.findById(Integer.parseInt(params.get("game").toString().substring(params.get("game").toString().size() -1)))
        def tag = params.get("tagName")
        def find = Tag.findByTitle(tag)
        if(find) {
            find.addToGames(game)
        } else {
            def newTag = new Tag(title: tag)
            newTag.addToGames(game)
            newTag.save(flush: true, failOnError: true)
        }
        render view: 'browse', model: [dbGames: Game.all, user: userLoginService.getUser(), userGames: userLoginService.getUser().games]
    }

    def addGameToUser() {
        def game = Game.findById(Integer.parseInt(params.get("game").toString().substring(params.get("game").toString().size() -1)))
        println "params: " + params.get("game").toString()
        User user = userLoginService.getUser()
        if(user.games) {
            user.addToGames(game)
        } else {
            user.games = new ArrayList()
            user.addToGames(game)
        }
        user.save(flush: true, failOnError: true)
        render(view: '../game/browse', model: [userGames: user.games, user: user, dbGames: Game.all])
    }
}
