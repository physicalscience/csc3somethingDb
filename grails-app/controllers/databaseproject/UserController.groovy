package databaseproject

class UserController {
    def userLoginService

    def index() { }

    def loadFront() {
        def user = userLoginService.getUser()
        if(user) {
            render(view: '../index', model: [user: user, loginClicked: false])
        } else {
            render(view: '../index', model: [user: null, loginClicked: false])
        }
    }

    def loginPage() {
        def user = userLoginService.getUser()
        if(user) {
            render(view: '../index', model: [exists: false, badLogin: false, user: user, loginClicked: true])
        } else {
            render(view: '../index', model: [exists: false, badLogin: false, user: null, loginClicked: true])
        }

    }

    def login() {
        def email = params.get("email")
        def password = params.get("password")
        def user = User.findByEmail(email.toString())
        if(user) {
            if(user.password == password.toString()) {
                userLoginService.setUser(user)
                render(view: '../index', model: [user: user])
            } else {
                render(view: 'loginPage', model: [exists: false, badLogin: true])
            }
        } else {
            render(view: 'loginPage', model: [exists: false, badLogin: true])
        }
    }

    def createUser() {
        render(view: 'newUserPage')
    }

    def newUser() {
        def email = params.get("email")
        def password = params.get("password")
        def description = params.get("description")

        if(User.findByEmail(email.toString())) {
            render(view: 'loginPage', model: [exists: true, badLogin: false])
        } else {
            def newUser = new User(email: email.toString(), password: password.toString(), description: description.toString())
            newUser.save(flush: true, failOnError: true)
            userLoginService.setUser(newUser)
            render(view: '../index', model: [user: newUser])
        }
    }

}
