package databaseproject

import grails.gorm.transactions.Transactional

@Transactional
class UserLoginService {
    static scope = 'session'
    def user = null

    def serviceMethod() {

    }

    def setUser(User user) {
        this.user = user
    }

    def getUser() {
        user
    }
}
