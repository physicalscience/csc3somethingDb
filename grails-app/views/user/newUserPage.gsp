<!doctype html>
<html>
    <head>
        <meta name="layout" content="main"/>
        <title>New User Son</title>
    </head>
    <body>
        <g:form controller="user" action="newUser">
            <label>Enter an email:</label>
            <g:textField name="email"/>
            <label>Enter a password:</label>
            <g:passwordField name="password"/>
            <label>Say a little about yourself!</label>
            <g:textArea name="description"/>
            <g:actionSubmit value="newUser"/>
        </g:form>
    </body>
</html>