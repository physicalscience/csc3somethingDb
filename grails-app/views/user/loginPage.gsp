<!doctype html>
<html>
    <head>
        <meta name="layout" content="main"/>
        <title>Logging in Son</title>
    </head>
    <body>
    <g:if test="${exists}">
        <h3>That email already exists in the database!</h3>
    </g:if>
    <g:if test="${badLogin}">
        <h3>The Username or password was incorrect!</h3>
    </g:if>
    <g:if test="${user}">
        <h3>Currently logged in as ${user.email}</h3>
    </g:if>
    <g:form controller="user" action="login">
        <label>Email</label>
        <g:textField name="email"/>
        <label>Password</label>
        <g:passwordField name="password"/>
        <g:actionSubmit value="login"/>
    </g:form>
    <g:link controller="user" action="createUser">Not a user yet?</g:link>
    </body>
</html>