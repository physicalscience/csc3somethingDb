<!doctype html>
<html lang="en" class="no-js">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>;PL'
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link type="text/css" href="${resource(dir: 'css', file: 'index-css.css')}" rel="stylesheet"/>
    <g:layoutHead/>
</head>
<body>
    <nav>
        <ul>
            <li><g:link controller="user" action="loadFront">Home</g:link></li>
            <li><g:link controller="game" action="loadSearch">Search</g:link></li>
            <li><g:link controller="game" action="loadBrowse">Browse</g:link></li>
            <li style="float:right"><g:link controller="user" action="loginPage" id="login">Login</g:link></li>
        </ul>
    </nav>
    <g:layoutBody/>
</body>
</html>
