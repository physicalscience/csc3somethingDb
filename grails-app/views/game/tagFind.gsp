<!doctype html>
<html>
    <head>
        <meta name="layout" content="main"/>
        <title>Tagging son</title>
    </head>
    <body>
        <g:form controller="game" action="addTag" params="[game: game]">
            <label>Add a tag!</label>
            <g:textField name="tagName"/>
            <g:actionSubmit value="Submit" action="addTag"/>
        </g:form>
    </body>
</html>