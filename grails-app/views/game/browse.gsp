<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Browsing son</title>
</head>
<body>
    <div class="main-body">
        <g:if test="${dbGames.size() == 0}">
            <h2>There are no games saved yet you bozo!</h2>
        </g:if>
        <g:else>
            <g:each var="${g}" in="${dbGames}">
                <h2>Name: ${g.name}</h2>
                <p>Description: ${g.deck}</p>
                <g:if test="${g.tags}">
                    <h3>Tags:</h3>
                    <g:each var="${t}" in="${g.tags}">
                        <p>${t.title}</p>
                    </g:each>
                </g:if>
                <g:form controller="game" action="addGameToUser" params="[game: g, dbGames: dbGames]">
                    <g:if test="${user != null && (user.games == null || !user.games.contains(g))}">
                        <g:actionSubmit value="Add Game" action="addGameToUser"/>
                    </g:if>
                    <g:actionSubmit value="Add Tag" action="tagPage"/>
                </g:form>
                <hr>
            </g:each>
        </g:else>
    </div>
</body>
</html>