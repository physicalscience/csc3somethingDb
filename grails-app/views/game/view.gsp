<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Searching son</title>
</head>
<body>
    <div class="main-body">
        <g:form controller="game" action="search">
            <label>Search for a game</label>
            <g:textField name="gameName"/>
            <g:actionSubmit value="search"/>
        </g:form>
    </div>
</body>
</html>