<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>game found boy</title>
</head>
<body>
    <div class="main-body">
        <g:if test="${game == null}">
            <h1>No results in the database, but here are some web results!</h1>
            <g:each var="${g}" in="${games}">
                <h2>Name: ${g.name}</h2>
                <p>Description: ${g.deck}</p>
                <g:each var="${p}" in="${g.platforms}">
                    <h3>On: ${p.name}</h3>
                </g:each>
            </g:each>
        </g:if>
        <g:else>
            <h1>Found!</h1>
            <h2>Name: ${game.name}</h2>
            <p>Description: ${game.deck}</p>
            <g:each var="${p}" in="${game.platforms}">
                <h3>On: ${p.title}</h3>
            </g:each>
        </g:else>
    </div>
</body>
</html>