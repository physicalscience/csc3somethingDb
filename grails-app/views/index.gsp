<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Welcome to this game thing</title>
    <link type="text/css" href="${resource(dir: 'css', file: 'login.css')}" rel="stylesheet"/>
</head>
<body>
    <header>
        <g:img dir="images" file="Mario mushroom wallpaper - Vector wallpapers - 26595.jpg"
               height="100%" width="100%"/>
    </header>
    <g:if test="${loginClicked}">
        <div class="box">
            <input type="close" name="" value="X">
            <h2>Login</h2>
            <g:form controller="user" action="login">
                <div class="inputBox">
                    <g:textField name="email"/>
                    <label>Username</label>
                </div>
                <div class="inputBox">
                    <g:passwordField name="password"/>
                    <label>Password</label>
                </div>
                <g:actionSubmit value="login" />
                <div class="newUser">
                    <g:link controller="user" action="createUser">Not a user yet?</g:link>
                </div>
            </g:form>
        </div>
    </g:if>





        <div class="box">
            <input type="close" name="" value="X">
            <h2>Signup</h2>
            <div class="inputBox">
                <input type="text" name="" required="">
                <label>Username</label>
            </div>
            <div class="inputBox">
                <input type="password" name="" required="">
                <label>Password</label>
            </div>
            <div class="inputBox">
                <input type="summary" name="" required="">
                <label>Say a little bit about yourself!</label>
            </div>
            <input type="submit" name="" value="Register" >
        </div>









    <div class="main-body">
        <h2>Hello, welcome to this website thing</h2>
        <g:if test="${user != null}">
            <h3>Logged in as ${user.email}</h3>
        </g:if>
        <p>Hi, this is a website that lets you look up information about video games. This website also lets
        you use a cosine similarity metric to discover other games you might be interested in.</p>
        <p>I'm Mr. scientist man, therefore you can trust me.</p>
    </div>
</body>
</html>
