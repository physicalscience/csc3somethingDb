package databaseproject

class Genre {
    String title
    static hasMany = [games: Game]
    static constraints = {
        games nullable: true
    }
}
