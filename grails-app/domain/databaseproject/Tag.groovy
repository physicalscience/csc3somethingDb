package databaseproject

class Tag {
    String title

    static hasMany = [games: Game]
    static constraints = {
        games nullable: true
    }
}
