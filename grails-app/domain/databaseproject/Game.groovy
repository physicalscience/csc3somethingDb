package databaseproject

class Game {
    String name
    Date release
    String deck
    static hasMany = [platforms: Platform, tags: Tag]
    static hasOne = [genre: Genre]
    static constraints = {
        release nullable: true
        tags nullable: true
    }
}
