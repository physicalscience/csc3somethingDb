package databaseproject

class User {
    String email
    String password
    String description

    static hasMany = [games: Game]

    static constraints = {
        email email: true
        games nullable: true
    }
}
